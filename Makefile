# Minimal makefile for Sphinx documentation
#

# You can set these variables from the command line, and also
# from the environment for the first two.
SPHINXOPTS    ?=
SPHINXBUILD   ?= sphinx-build
SOURCEDIR     = .
BUILDDIR      = _build

# Internal variables.
PAPEROPT_a4     = -D latex_paper_size=a4
PAPEROPT_letter = -D latex_paper_size=letter
ALLSPHINXOPTS   = -d $(BUILDDIR)/doctrees $(PAPEROPT_$(PAPER)) $(SPHINXOPTS) .

# Put it first so that "make" without argument is like "make help".
help:
	@echo "Please use \`make <target>' where <target> is one of"
	@echo "  clean      to remove any build files preset"
	@echo "  html       to make standalone HTML files"
	@echo "  pdf        to make pdf using xelatex"
	@echo "  linkcheck  to check all external links for integrity"
	@echo "  doctest    to run all doctests embedded in the documentation (if enabled)"

.PHONY: help clean check_sphinx-build html changes spelling linkcheck Makefile

clean:
	rm -rf $(BUILDDIR)/*

# User-friendly check for sphinx-build
check_sphinx-build:
	@which $(SPHINXBUILD) >/dev/null 2>&1 || (echo "The '$(SPHINXBUILD)' command was not found. Make sure you have Sphinx installed, then set the SPHINXBUILD environment variable to point to the full path of the '$(SPHINXBUILD)' executable. Alternatively you can add the directory with the executable to your PATH. If you don't have Sphinx installed, grab it from http://www.sphinx-doc.org/en/master/)" >&2; false)

html: check_sphinx-build
	$(SPHINXBUILD) -T -b html $(ALLSPHINXOPTS) $(BUILDDIR)/html \
              || (echo "sphinx-build failed $$?"; exit 1)
	@echo
	@echo "Build finished. The HTML pages are in $(BUILDDIR)/html."

spelling: check_sphinx-build
	$(SPHINXBUILD) -T -b spelling $(ALLSPHINXOPTS) $(BUILDDIR)/spelling \
              || (echo "sphinx-build failed $$?"; exit 1)
	@echo
	@echo "Spell check complete; look for any errors in the above output " \
	      "or in $(BUILDDIR)/spelling/output.txt."

linkcheck: check_sphinx-build
	$(SPHINXBUILD) -T -b linkcheck $(ALLSPHINXOPTS) $(BUILDDIR)/linkcheck \
              || (echo "sphinx-build failed $$?"; exit 1)
	@echo
	@echo "Link check complete; look for any errors in the above output " \
	      "or in $(BUILDDIR)/linkcheck/output.txt."

# Catch-all target: route all unknown targets to Sphinx using the new
# "make mode" option.  $(O) is meant as a shortcut for $(SPHINXOPTS).
%: Makefile
	@$(SPHINXBUILD) -M $@ "$(SOURCEDIR)" "$(BUILDDIR)" $(SPHINXOPTS) $(O)
