================
The Idem Project
================

https://gitlab.com/vmware/idem/idem
Under Construction

Welcome to the Idem project! Our goal here is to develop a new tool for managing and enforcing idempotency for your cloud infrastructure.


While idempotence is a funky word that can sometimes a confusing concept, at least from the academic definition, it's very easy to understand.
----------------------------------------------------------------------------------------------------------------------------------------------

To really understand it's power, let's see of how idempotency can help From a RESTful service standpoint.
For an operation to be considered idempotent, clients can make that same call repeatedly while producing the same result. In other words, making multiple identical requests has the same effect as making a single request. While idempotent operations produce the same result on the server, the response itself may not be the same, such as a resource's state may change between requests.

The PUT and DELETE methods are defined to be idempotent also. The aforementioned response/request situation can be best understood through the DELETE operation - while the first call can return a 200 (OK), subsequent calls will return a 400 (BAD), indicating it's attempting to access a resource that no longer exists. While the state on the server is still correct, the response from the request could indicate otherwise.

Idem as a project ties into this concept of idempotency, hence the name, to deliver the same robust concept into a CLI tool.
-----------------------------------------------------------------------------------------------------------------------------
The heart of Idem are the state functions ``Describe Present Absent``. These three functions allow you to apply the idempotency concepts to your cloud resources.

``Describe`` will retrieve the current state of a cloud resource and return to you all of the necessary information about that resource in order to fully manage it.

``Present`` will use that same information to maintain the idempotency nature of that resource. For ease of use, this information is stored as yaml markup. From here, each attribute can be edited and given back to the Present function as input data to put your cloud resource into that new state.

``Absent`` will allow users to destroy, or remove, a cloud resource from a server.

Idem-Gitlab - A use case scenario.
----------------------------------

Sometimes it's best to see something in action to understand how powerful it is.

Given a gitlab environment that has a project initialized, we can run the command:
``idem describe "gitlab.project.projects*"``
and below is a snippet of the output:
    project.projects-30690183:
        gitlab.project.projects.present:
            - name: test-project
            - path: test-project
            - allow_merge_on_skipped_pipeline: null
            - analytics_access_level: enabled
            - approvals_before_merge: 0
            - auto_cancel_pending_pipelines: enabled
            - auto_devops_deploy_strategy: continuous
            - container_expiration_policy_attributes: null
            - container_registry_enabled: true
            - container_registry_access_level: enabled
            - default_branch: main
            - description: Sample Description
            - external_authorization_classification_label: ''
            - forking_access_level: enabled
            - initialize_with_readme: null
            - issues_access_level: enabled
            - merge_method: merge
            - merge_requests_access_level: enabled
            - operations_access_level: enabled
            - only_allow_merge_if_all_discussions_are_resolved: false
            - only_allow_merge_if_pipeline_succeeds: false

This can be saved into a .sls file called output.sls and then manipulate it manually or programmatically. Let's focus on a single element, description.

          - description: Sample Description

We can make a quick modification to this element and have description now read as:

          - description: Updated description using Idem

So that our new .sls file looks like this:
    project.projects-30690183:
        gitlab.project.projects.present:
            - name: test-project
            - path: test-project
            - allow_merge_on_skipped_pipeline: null
            - analytics_access_level: enabled
            - approvals_before_merge: 0
            - auto_cancel_pending_pipelines: enabled
            - auto_devops_deploy_strategy: continuous
            - container_expiration_policy_attributes: null
            - container_registry_enabled: true
            - container_registry_access_level: enabled
            - default_branch: main
            - description: **Updated description using Idem**
            - external_authorization_classification_label: ''
            - forking_access_level: enabled
            - initialize_with_readme: null
            - issues_access_level: enabled
            - merge_method: merge
            - merge_requests_access_level: enabled
            - operations_access_level: enabled
            - only_allow_merge_if_all_discussions_are_resolved: false
            - only_allow_merge_if_pipeline_succeeds: false

We can now apply this state using:
``idem state output.sls``
and our changes will reflect in the project with our new description

This quick example is to show truly how quickly idem can describe the state of a resource and manage it in a stateful way through idempotency concepts.
